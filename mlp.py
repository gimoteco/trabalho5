#-*- coding: utf-8 -*-
import math

BIAS = [-1]
CASAS_DECIMAIS = 4
TAXA_DE_APRENDIZAGEM = 0.1
NUMERO_DE_EPOCAS = 101 # 101 POIS COMECEI C/ O MODELO SEM TREINAR

INSTANCIAS = [ 
	(1, 1, 1, 0,),
	(1.8, 1.5, 1, 0,),
	(1, 2, 1, 0, ),
	(1.8, 2.5, 1, 0, ),
	(2, 1, 1, 0, ),
	(3, 2.5, 1, 0, ),
	(2.5, 1.5, 0, 1,),
	(3.5, 2.3, 0, 1, ),
	(3, 3, 0, 1, ),
	(4, 2.5, 0, 1,), 
	(4, 3, 0, 1, ),
	(4.5, 3.5, 0, 1, ),
]

def funcao_de_ativacao_camada_oculta(x):
	return 1.0/(1+ pow(math.e, -x))

def funcao_de_ativacao_camada_de_saida(x):
	return x

def somatorio(entrada, pesos):
	obtido = 0.0
	for i, j in zip(entrada, pesos):
		obtido += i*j
	return obtido

def erro(esperado, obtido):
	return esperado - obtido

def funcao_derivada_da_camada_oculta(x):
	return x*(1-x)

def funcao_derivada_da_camada_saida(x):
	return 1

def ajuste(peso_entre_X_e_Y, saida_do_no_y, saida_esperada, entrada):
	return peso_entre_X_e_Y * funcao_derivada_da_camada_saida(saida_do_no_y) * erro(esperado=saida_esperada, obtido=saida_do_no_y) * entrada

#!!!!!!!!!!!! ALTERAR quando pegar as INSTANCIAS


pesos_e = 0.2, -0.1, 0.3
pesos_f = -0.5, -0.8, 0.4
pesos_g = -0.1, 0.1, -0.6

pesos_h = 0.1, 0.5, -0.3, 0.1
pesos_i = 0.3, 0.2, 0.6, -0.9

pesos_camada_de_entrada = [pesos_e, pesos_f, pesos_g]
pesos_camada_oculta = [pesos_h, pesos_i]

for epoca in range(NUMERO_DE_EPOCAS):
	print '='* 10 , 'Epóca {}'.format(epoca), '=' * 10
	for instancia in INSTANCIAS:
		entrada = BIAS + [instancia[0], instancia[1]]
		saidas_camada_oculta = []
		saidas_camada_de_saida = []

		#!!!!!!!!!!!! ALTERAR quando pegar as INSTANCIAS
		saida_esperada = [instancia[2], instancia[3]]

		# Cálculo entrada para a camada oculta
		for pesos in pesos_camada_de_entrada:
			saida = funcao_de_ativacao_camada_oculta(somatorio(entrada, pesos))
			saidas_camada_oculta.append(round(saida, CASAS_DECIMAIS))

		# Cálculo da camada oculta para a camada de saida
		for pesos in pesos_camada_oculta:
			entrada_camada_de_saida = BIAS + saidas_camada_oculta
			saida = funcao_de_ativacao_camada_de_saida(somatorio(entrada_camada_de_saida, pesos))
			saidas_camada_de_saida.append(round(saida, CASAS_DECIMAIS))

		# Ajustes da camada oculta para camada de saída
		t_saidas = []
		nv_peso_saida = []
		for n_saida, saida in enumerate(saidas_camada_de_saida) :
			for n_peso, peso in enumerate(pesos_camada_oculta[n_saida]):
				print "Peso -> {}\nSaida -> {}\nEsperada -> {}\nEntrada -> {}".format(peso, saida, saida_esperada[n_saida], entrada_camada_de_saida[n_peso])
				vlr_ajuste = ajuste(TAXA_DE_APRENDIZAGEM, saida, saida_esperada[n_saida], entrada_camada_de_saida[n_peso])
				print "Ajuste -> ", ajuste(TAXA_DE_APRENDIZAGEM, saida, saida_esperada[n_saida], entrada_camada_de_saida[n_peso])
				print 'Novo valor -> {} + {} = {}'.format(peso, vlr_ajuste, peso + vlr_ajuste)
				print '\n'*2
				nv_peso_saida.append(peso+vlr_ajuste)
			t_saidas.append(funcao_derivada_da_camada_saida(saida) * erro(saida_esperada[n_saida], saida))

		#print t_saidas

		nv_peso_oculta = []
		# Ajustes da camada de entrada para a camada de saída
		for n_peso, peso in enumerate(pesos_camada_de_entrada):
			for j_peso, peso2,in enumerate(entrada_camada_de_saida[1:]):

				print 'Entrada->{}\nPeso->{}\nSaída->{}\nT(S1)->{}\nPesoAtéS1(1)->{}\nT(S2)->{}\nPesoAtéS1(2)->{}'.format(entrada[j_peso], \
					peso[j_peso], entrada_camada_de_saida[n_peso+1], \
					t_saidas[0], pesos_camada_oculta[0][1:][n_peso], \
					t_saidas[1],  pesos_camada_oculta[1][1:][n_peso])

				vlr_ajuste = TAXA_DE_APRENDIZAGEM * \
					(t_saidas[0]*pesos_camada_oculta[0][1:][n_peso] + t_saidas[1]*pesos_camada_oculta[1][1:][n_peso]) * \
					entrada[j_peso] * \
					funcao_derivada_da_camada_oculta(entrada_camada_de_saida[n_peso+1])
				print 'Novo valor -> {} + {} = {}'.format(peso[j_peso], vlr_ajuste, peso[j_peso] + vlr_ajuste)
				nv_peso_oculta.append(peso[j_peso]+vlr_ajuste)

				print '\n'*2


		#print '\n'*2, pesos_camada_de_entrada, '\n\n',  nv_peso_oculta, '\n\n', pesos_camada_oculta,'\n', nv_peso_saida,
		pesos_camada_de_entrada = (nv_peso_oculta[0:3], nv_peso_oculta[3:6], nv_peso_oculta[6:])
		pesos_camada_oculta = (nv_peso_saida[0:4], nv_peso_saida[4:])

		print pesos_camada_de_entrada
		print pesos_camada_oculta
	print '='* 10 , 'FIM da epóca {}'.format(epoca), '=' * 10